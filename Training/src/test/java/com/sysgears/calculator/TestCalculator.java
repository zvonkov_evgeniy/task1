/**
 * Created with IntelliJ IDEA.
 * User: nick
 * Date: 7/10/13
 * Time: 3:46 PM
 * To change this template use File | Settings | File Templates.
 */

package test.java.com.sysgears.calculator;

import main.java.com.sysgears.calculator.*;
import java.util.*;


public class TestCalculator {

    public static void main(String[] args) {

        Calculator calc = new Calculator();

        Scanner scanner = new Scanner(System.in);

        //while (true) {
            System.out.print("> ");
            try {
                System.out.println("= " + calc.eval(scanner.nextLine()));
            } catch (EmptyArgument e) {
                System.out.println(e.getError());
            }  catch (IllegalArgumentException ex) {
                System.out.println("Invalid input expression");
            };
            //System.out.println("Exit? y/n");

        //}

        //scanner.close();
    }
}