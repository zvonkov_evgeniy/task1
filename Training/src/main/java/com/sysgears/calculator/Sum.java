/**
 * Created with IntelliJ IDEA.
 * User: nick
 * Date: 7/11/13
 * Time: 12:36 PM
 * To change this template use File | Settings | File Templates.
 */

package main.java.com.sysgears.calculator;


public class Sum implements Type {

    private char c = '+';

    public char getChar() {
        return c;
    }

    public int get(int x, int y){
        return x + y;
    }
}
