/**
 * Created with IntelliJ IDEA.
 * User: nick
 * Date: 7/9/13
 * Time: 9:33 AM
 * To change this template use File | Settings | File Templates.
 */

package main.java.com.sysgears.calculator;

import java.util.HashSet;
import java.util.Arrays;


public class Calculator {

    private HashSet<Character> operators
            = new HashSet<Character>(Arrays.asList('+', '-', '*'));

    public int eval(String expression) throws EmptyArgument {

        int value = 0;

        if (expression.isEmpty()) {
            throw new EmptyArgument();
        }

        expression = format(expression);

        try {
            expression = operation(expression, new Mult());
            expression = operation(expression, new Sum());
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException();
        }

        value = Integer.parseInt(expression);

        return value;
    }

    private String format(String arg) {

        arg = arg.replaceAll(" ", "");

        if ((arg.charAt(0) == '-') || (arg.charAt(0) == '+')) {
            arg = '0' + arg;
        };

        return arg;
    }

    private String operation(String arg, Type type) {

        int value = 0;

        int index = arg.indexOf(type.getChar());

        if (index != -1) {
            int begin = operandSearch(arg, index - 1, -1) + 1;
            int end = operandSearch(arg, index + 1, 1);

            value = type.get(Integer.parseInt(arg.substring(begin, index)),
                    Integer.parseInt(arg.substring(index + 1, end)));

             arg = arg.substring(0, begin) + value
                    + arg.substring(end, arg.length());

            arg = operation(arg, type);
        };

        return arg;
    }

    private int operandSearch(String arg, int index, int inc) {
        if ((index >= 0) & (index < arg.length())
                && !(this.operators.contains(arg.charAt(index)))) {
            index = operandSearch(arg, index + inc, inc);
        }
        return index;
    }

}
