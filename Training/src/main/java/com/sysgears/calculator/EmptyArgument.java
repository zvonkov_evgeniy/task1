/**
 * Created with IntelliJ IDEA.
 * User: nick
 * Date: 7/11/13
 * Time: 10:34 AM
 * To change this template use File | Settings | File Templates.
 */

package main.java.com.sysgears.calculator;


public class EmptyArgument extends IllegalArgumentException {

    private String massage;

    public EmptyArgument() {
        this.massage = "Empty input expression.";
    }

    public EmptyArgument(String errorMessage) {
        this.massage = errorMessage;
    }

    public String getError() {
        return this.massage;
    }
}
