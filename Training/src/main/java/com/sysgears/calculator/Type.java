/**
 * Created with IntelliJ IDEA.
 * User: nick
 * Date: 7/11/13
 * Time: 12:27 PM
 * To change this template use File | Settings | File Templates.
 */

package main.java.com.sysgears.calculator;


public interface Type {

    public char getChar();

    public int get(int x, int y);

}
